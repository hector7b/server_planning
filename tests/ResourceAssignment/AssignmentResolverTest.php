<?php
declare(strict_types=1);

use Hector7b\ResourceAssignment\AssignmentResolver;
use Hector7b\ResourceAssignment\Server;
use Hector7b\ResourceAssignment\VirtualMachine;
use Hector7b\ResourceAssignment\VirtualMachineCantBeAssignedException;
use PHPUnit\Framework\TestCase;

class AssignmentResolverTest extends TestCase
{
    public function testCanBeCreatedWithValidParameters() : void
    {
        $this->assertInstanceOf(AssignmentResolver::class, new AssignmentResolver());
    }

    public function testGetPartiallyAssignedServers() : void
    {
        $assignmentResolver = new AssignmentResolver();

        $this->assertIsArray($assignmentResolver->getPartiallyAssignedServers());
    }

    public function testExceptionThrownForInvalidParameters() : void
    {
        $this->expectException(TypeError::class);

        $assignmentResolver = new AssignmentResolver();
        $assignmentResolver->calculate(2, 0);
    }

    public function testExceptionThrownForInvalidNumberOfVirtualMachines() : void
    {
        $this->expectException(TypeError::class);

        $server = new Server(2, 8, 50);

        $assignmentResolver = new AssignmentResolver();
        $assignmentResolver->calculate($server, []);
    }

    public function testExceptionThrownForNonAssignableVirtualMachines() : void
    {
        $this->expectException(VirtualMachineCantBeAssignedException::class);

        $server = new Server(2, 8, 50);
        $virtualMachines = [
            new VirtualMachine(1,4,10),
            new VirtualMachine(4, 2,5),
        ];

        $assignmentResolver = new AssignmentResolver();
        $assignmentResolver->calculate($server, ...$virtualMachines);
    }

    public function testPartialServerAssignment() : void
    {
        $server = new Server(2, 8, 50);
        $virtualMachines = [
            new VirtualMachine(1,4,10),
        ];

        $assignmentResolver = new AssignmentResolver();
        $numberOfServers = $assignmentResolver->calculate($server, ...$virtualMachines);
        $partialServers = $assignmentResolver->getPartiallyAssignedServers();

        $this->assertCount(1, $partialServers);
        $this->assertEquals([
            'cpu' => 1,
            'ram' => 4,
            'hdd' => 40,
        ], $partialServers[0]);

        $this->assertEquals(1, $numberOfServers);
    }

    /**
     * @dataProvider calculateDataProvider
     */
    public function testCalculate(Server $server, array $virtualMachines, int $expectedCount) : void
    {
        $assignmentResolver = new AssignmentResolver();

        $numberOfServers = $assignmentResolver->calculate($server, ...$virtualMachines);

        $this->assertEquals($expectedCount, $numberOfServers);
    }

    public function calculateDataProvider() : array
    {
        return [
            [
                new Server(1,4,10),
                [
                    new VirtualMachine(1,4,10)
                ],
                1,
            ],
            [
                new Server(2, 8, 20),
                [
                    new VirtualMachine(1, 4, 10),
                    new VirtualMachine(1, 4, 10),
                ],
                1,
            ],
            [
                new Server(16, 32, 1024),
                [
                    new VirtualMachine(1, 4, 10),
                    new VirtualMachine(1, 4, 10),
                ],
                1,
            ],
            [
                new Server(2, 8, 20),
                [
                    new VirtualMachine(2, 4, 10),
                    new VirtualMachine(1, 4, 10),
                ],
                2,
            ],
            [
                new Server(2, 8, 20),
                [
                    new VirtualMachine(1, 4, 10),
                    new VirtualMachine(1, 4, 10),
                    new VirtualMachine(2, 8, 20),
                ],
                2,
            ],
            [
                new Server(1, 4, 10),
                [
                    new VirtualMachine(1, 4, 10),
                    new VirtualMachine(1, 4, 10),
                    new VirtualMachine(1, 4, 10),
                ],
                3,
            ],
        ];
    }
}