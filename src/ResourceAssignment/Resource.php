<?php
declare(strict_types=1);

namespace Hector7b\ResourceAssignment;

abstract class Resource
{
    /**
     * @var int
     */
    public $cpu;

    /**
     * @var int
     */
    public $ram;

    /**
     * @var int
     */
    public $hdd;

    /**
     * Resource constructor.
     * @param int $cpu
     * @param int $ram
     * @param int $hdd
     */
    public function __construct(int $cpu, int $ram, int $hdd)
    {
        if (!self::paramValuesAreValid($cpu, $ram, $hdd)) {
            throw new \InvalidArgumentException('Invalid value for resource');
        };

        $this->cpu = $cpu;
        $this->ram = $ram;
        $this->hdd = $hdd;
    }

    private static function paramValuesAreValid(int $cpu, int $ram, int $hdd) : bool
    {
        return ($cpu > 0 && $ram > 0 && $hdd > 0);
    }
}