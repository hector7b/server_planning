<?php
declare(strict_types=1);

namespace Hector7b\ResourceAssignment;

class Server extends Resource
{
    /**
     * Server constructor.
     * @param int $cpu
     * @param int $ram
     * @param int $hdd
     */
    public function __construct(int $cpu, int $ram, int $hdd)
    {
        parent::__construct($cpu, $ram, $hdd);
    }

    /**
     * @return array
     */
    public function getCurrentState() : array
    {
        return get_object_vars($this);
    }

    /**
     * @param int $cpu
     * @param int $ram
     * @param int $hdd
     */
    public function allocate(int $cpu, int $ram, int $hdd) : void
    {
        $this->cpu -= $cpu;
        $this->ram -= $ram;
        $this->hdd -= $hdd;
    }

    /**
     * @return bool
     */
    public function isFull() : bool
    {
        return $this->cpu === 0 || $this->ram === 0 || $this->hdd === 0;
    }
}