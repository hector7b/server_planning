<?php
declare(strict_types=1);

use Hector7b\ResourceAssignment\Server;
use PHPUnit\Framework\TestCase;

class ServerTest extends TestCase
{
    public function testCanBeCreatedWithValidParameters() : void
    {
        $this->assertInstanceOf(Server::class, new Server(1,2,4));
    }

    public function testCantBeCreatedWithInvalidParameterTypes() : void
    {
        $this->expectException(TypeError::class);

        $server = new Server(1,2,4.0);
    }

    public function testCantBeCreatedWithInvalidParameterValues() : void
    {
        $this->expectException(InvalidArgumentException::class);

        $server = new Server(1,-2,4);
    }

    public function testGetCurrentState() : void
    {
        $server = new Server(1,2,4);

        $this->assertEquals([
            'cpu' => 1,
            'ram' => 2,
            'hdd' => 4
        ], $server->getCurrentState());
    }

    /**
     * @dataProvider allocateDataProvider
     */
    public function testAllocate(array $serverValues, array $virtualMachineValues, array $expectedValues) : void
    {
        $server = new Server($serverValues[0],$serverValues[1],$serverValues[2]);

        $server->allocate($virtualMachineValues[0], $virtualMachineValues[1], $virtualMachineValues[2]);

        $this->assertEquals($server->cpu, $expectedValues[0]);
        $this->assertEquals($server->ram, $expectedValues[1]);
        $this->assertEquals($server->hdd, $expectedValues[2]);
    }

    public function testIsFull() : void
    {
        $server = new Server(1, 2, 3);

        $this->assertFalse($server->isFull());

        $server->allocate(1,1,1);

        $this->assertTrue($server->isFull());
    }

    public function allocateDataProvider() : array
    {
        return [
            [
                [2, 4, 8],
                [1, 2, 4],
                [1, 2, 4],
            ],
            [
                [2, 4, 8],
                [2, 4, 8],
                [0, 0, 0],
            ],
            [
                [4, 16, 100],
                [4, 8, 10],
                [0, 8, 90],
            ],
        ];
    }
}
