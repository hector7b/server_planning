<?php
declare(strict_types=1);

namespace Hector7b\ResourceAssignment;

use function foo\func;

class AssignmentResolver
{
    private $partiallyAssignedServers = [];
    private $count = 0;
    private $serverType;

    public function __construct(){}

    /**
     * @param Server $serverType
     * @param VirtualMachine ...$virtualMachines
     * @return int
     * @throws VirtualMachineCantBeAssignedException
     */
    public function calculate(Server $serverType, VirtualMachine ...$virtualMachines) : int
    {
        if (count($virtualMachines) === 0){
            throw new \InvalidArgumentException('The list of virtual machines cannot be empty');
        }

        $this->serverType = $serverType;

        foreach ($virtualMachines as $virtualMachine) {
            $this->assignResource($virtualMachine);
        }

        return $this->count;
    }

    /**
     * @return array
     */
    public function getPartiallyAssignedServers() : array
    {
        return array_map(function(Server $server) {
            return $server->getCurrentState();
        }, $this->partiallyAssignedServers);
    }

    /**
     * @param VirtualMachine $virtualMachine
     * @throws VirtualMachineCantBeAssignedException
     */
    private function assignResource(VirtualMachine $virtualMachine) : void
    {
        if (!$this->virtualMachineIsAssignable($virtualMachine, $this->serverType)) {
            throw new VirtualMachineCantBeAssignedException();
        }

        $this->allocateServer($virtualMachine);
    }

    /**
     * @param VirtualMachine $virtualMachine
     * @param Server $server
     * @return bool
     */
    private function virtualMachineIsAssignable(VirtualMachine $virtualMachine, Server $server) : bool
    {
        return $virtualMachine->cpu <= $server->cpu &&
            $virtualMachine->ram <= $server->ram &&
            $virtualMachine->hdd <= $server->hdd;
    }

    /**
     * @param VirtualMachine $virtualMachine
     */
    private function allocateServer(VirtualMachine $virtualMachine) : void
    {
        if ($this->allocatedInPartiallyAssignedServers($virtualMachine)) {
            return;
        }

        $server = new Server(
            $this->serverType->cpu,
            $this->serverType->ram,
            $this->serverType->hdd
        );

        $this->count++;

        $server->allocate($virtualMachine->cpu, $virtualMachine->ram, $virtualMachine->hdd);

        if (!$server->isFull()) {
            $this->partiallyAssignedServers[] = $server;
        }
    }

    /**
     * @param VirtualMachine $virtualMachine
     * @return bool
     */
    private function allocatedInPartiallyAssignedServers(VirtualMachine $virtualMachine) : bool
    {
        for ($i = 0; $i < count($this->partiallyAssignedServers); $i++) {
            $server = $this->partiallyAssignedServers[$i];

            if ($this->virtualMachineIsAssignable($virtualMachine, $server)) {
                $server->allocate($virtualMachine->cpu, $virtualMachine->ram, $virtualMachine->hdd);

                if ($server->isFull())
                {
                    unset($this->partiallyAssignedServers[$i]);
                }

                return true;
            }
        }

        return false;
    }
}