<?php
declare(strict_types=1);

use Hector7b\ResourceAssignment\VirtualMachine;
use PHPUnit\Framework\TestCase;

class VirtualMachineTest extends TestCase
{
    public function testCanBeCreatedWithValidParameters() : void
    {
        $this->assertInstanceOf(VirtualMachine::class, new VirtualMachine(1,2,4));
    }

    public function testCantBeCreatedWithInvalidParameterTypes() : void
    {
        $this->expectException(TypeError::class);

        $virtualMachine = new VirtualMachine(1,2,4.0);
    }

    public function testCantBeCreatedWithInvalidParameterValues() : void
    {
        $this->expectException(InvalidArgumentException::class);

        $virtualMatchine = new VirtualMachine(1,-2,4);
    }
}
