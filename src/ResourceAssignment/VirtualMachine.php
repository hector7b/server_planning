<?php
declare(strict_types=1);

namespace Hector7b\ResourceAssignment;

class VirtualMachine extends Resource
{
    /**
     * VirtualMachine constructor.
     * @param int $cpu
     * @param int $ram
     * @param int $hdd
     */
    public function __construct(int $cpu, int $ram, int $hdd)
    {
        parent::__construct($cpu, $ram, $hdd);
    }
}