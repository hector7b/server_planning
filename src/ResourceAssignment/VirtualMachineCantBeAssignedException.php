<?php
/**
 * Created by PhpStorm.
 * User: hacosta
 * Date: 2019-02-23
 * Time: 16:31
 */

namespace Hector7b\ResourceAssignment;

use Throwable;

class VirtualMachineCantBeAssignedException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->message = "The virtual machine needs more resources than the ones the server is able to provide";

        parent::__construct($message, $code, $previous);
    }
}